import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {

  double _valorSlider = 100;
  bool _bloquearCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sliders'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 50),
        child: Column(
          children: <Widget>[
            _crearSlider(),
            _sliderCheckBox(),
            _crearSwitch(),
            Expanded(child: _crearImagen())
          ],
        ),
      )
    );
  }

  Widget _crearSlider() {

    return Slider(
      activeColor: Colors.indigoAccent,
      label: 'Tamaño de la imagen',
      // divisions: 20,
      value: _valorSlider,
      min: 10,
      max: 400,
      onChanged: (_bloquearCheck) ? null : 
      (value){
        setState(() {
          _valorSlider = value; 
          });
        },
    );

  }

  Widget _sliderCheckBox() {
    // return Checkbox(
    //   value: _bloquearCheck,
    //   onChanged: (value){
    //     setState(() {
    //      _bloquearCheck = value; 
    //     });
    //   },
    // );

    return CheckboxListTile(
      title: Text('Bloquear slider'),
      value: _bloquearCheck,
      onChanged: (value){
        setState(() {
         _bloquearCheck = value; 
        });
      },
    );
  }

  Widget _crearSwitch() {

      return SwitchListTile(
      title: Text('Bloquear slider'),
      value: _bloquearCheck,
      onChanged: (value){
        setState(() {
         _bloquearCheck = value; 
        });
      },
    );
  }
  Widget _crearImagen() {
    return Image(
      image: NetworkImage('https://purepng.com/public/uploads/large/purepng.com-batman-arkham-knightbatmansuperherocomicdc-comicsbob-kanebat-manbruce-wayne-1701528525691kseuf.png'),
      width: _valorSlider,
      fit: BoxFit.contain,
    );
  }

  

  
}