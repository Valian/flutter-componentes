import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
   Widget _cardTipo1() {
      return Card(
        elevation: 10,
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(30)),
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.photo_album, color:Colors.blue),
              title: Text('Soy el titulo'),
              subtitle: Text('asdasdasdasdasdasdasdddddasdasdasdassssssssssssssssssssssaaaaaaaaaaaaasdasd'),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  child: Text('Cancelar'),
                  onPressed: (){},
                ),
                FlatButton(
                  child: Text('OK'),
                  onPressed: (){},
                ),
              ],
            )
          ],
        ),
      );
    }

Widget _cardTipo2() {
    return Card(
      elevation: 10,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder( borderRadius:  BorderRadius.circular(15.0) ),
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage('https://static.photocdn.pt/images/articles/2017_1/iStock-545347988.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration( milliseconds: 200 ),
            alignment: Alignment.center,
            height: 250.0,
            width: double.maxFinite,
            fit: BoxFit.cover,
          ),
          Container(
              padding: EdgeInsets.all(10.0), child: Text('Pie de foto')),
        ],
      ),
    );


    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards'),
        ),
        body: ListView(
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            _cardTipo1(),
            SizedBox(height: 30,),
            _cardTipo2(),
            SizedBox(height: 30,),
            _cardTipo1(),
            SizedBox(height: 30,),
            _cardTipo2(),
            SizedBox(height: 30,),
            _cardTipo1(),
            SizedBox(height: 30,),
            _cardTipo2(),
            SizedBox(height: 30,),
            _cardTipo1(),
            SizedBox(height: 30,),
            _cardTipo2(),
            SizedBox(height: 30,),
            _cardTipo1(),
            SizedBox(height: 30,),
            _cardTipo2(),
            SizedBox(height: 30,),
                      ],
                  ),
                );
              }
}